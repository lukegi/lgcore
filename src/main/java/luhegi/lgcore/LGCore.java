package luhegi.lgcore;

import luhegi.lgcore.config.ConfigManager;
import luhegi.lgcore.loading.ILoadableMod;
import luhegi.lgcore.loading.LoadingManager;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLDedicatedServerSetupEvent;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;

@Mod(LGCore.ID)
public class LGCore implements ILoadableMod {
    public static final String ID = "lgcore";

    public LGCore() {
        LoadingManager.INSTANCE.register(this);
        ConfigManager.INSTANCE.register(TestConfig.INSTANCE);
    }

    @Override
    public void onSetup(FMLCommonSetupEvent event) {

    }

    @Override
    @OnlyIn(Dist.CLIENT)
    public void onClientSetup(FMLClientSetupEvent event) {

    }

    @Override
    @OnlyIn(Dist.DEDICATED_SERVER)
    public void onServerSetup(FMLDedicatedServerSetupEvent event) {

    }

    @Override
    public void onGatherData(GatherDataEvent event) {

    }

    @Override
    public String getID() {
        return ID;
    }
}
