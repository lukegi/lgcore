package luhegi.lgcore;

import luhegi.lgcore.config.Config;
import luhegi.lgcore.config.IConfig;
import luhegi.lgcore.util.Log;
import net.minecraftforge.common.ForgeConfigSpec.IntValue;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.config.ModConfig.Type;

public final class TestConfig extends Config implements IConfig.EventHandler {
    public static final TestConfig INSTANCE = new TestConfig();

    public final IntValue test = builder().comment("test")
            .translation("lgcore.config.test.name")
            .defineInRange("test", 25, 0, 100);

    private TestConfig() {super(Type.SERVER);}

    @Override
    public EventHandler getHandler() {
        return this;
    }

    @Override
    public void onConfigLoad(ModConfig.Loading event) {
        Log.Info("Config Loaded");
    }

    @Override
    public void onConfigReload(ModConfig.ConfigReloading event) {
        Log.Info("Config Reloaded");
    }
}
