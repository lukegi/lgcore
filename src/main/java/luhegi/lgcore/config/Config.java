package luhegi.lgcore.config;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.common.ForgeConfigSpec.Builder;
import net.minecraftforge.fml.config.ModConfig.Type;

public abstract class Config implements IConfig {
    private Builder builder;
    private ForgeConfigSpec spec;
    private Type type;

    public Config(Type type) {
        this.type = type;
    }

    protected Builder builder() {
        if (builder == null) {
            builder = new Builder();
        }
        return builder;
    }

    @Override
    public ForgeConfigSpec getSpec() {
        if (spec == null) {
            spec = builder.build();
        }
        return spec;
    }

    @Override
    public Type getType() {
        return type;
    }
}
