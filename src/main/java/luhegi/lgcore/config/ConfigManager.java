package luhegi.lgcore.config;

import luhegi.lgcore.event.EventManager;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.config.ModConfig.Type;

public final class ConfigManager {
    public static final ConfigManager INSTANCE = new ConfigManager();

    private ConfigManager(){}

    public final void register(IConfig config) {
        final Type type = config.getType();
        final ForgeConfigSpec configSpec = config.getSpec();
        final IConfig.EventHandler handler = config.getHandler();

        ModLoadingContext.get().registerConfig(type, configSpec);
        if (handler != null) {
            EventManager.MOD_BUS.register(handler::onConfigLoad);
            EventManager.MOD_BUS.register(handler::onConfigReload);
        }
    }
}
