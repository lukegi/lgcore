package luhegi.lgcore.config;

import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.config.ModConfig.Type;

public interface IConfig {
    /**
     * The {@link Type type} of the config
     */
    Type getType();

    /**
     * The {@link ForgeConfigSpec config spec}
     */
    ForgeConfigSpec getSpec();

    /**
     * The {@link EventHandler event handler} for config events
     */
    EventHandler getHandler();

    interface EventHandler {
        /**
         * Called when the config is first loaded
         */
        void onConfigLoad(final ModConfig.Loading event);

        /**
         * Called when the config file is changed
         */
        void onConfigReload(final ModConfig.ConfigReloading event);
    }
}
