package luhegi.lgcore.event;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.Event;
import net.minecraftforge.eventbus.api.GenericEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

import java.util.function.Consumer;

public final class EventManager {
    public static final EventManager FORGE_BUS = new EventManager(MinecraftForge.EVENT_BUS);
    public static final EventManager MOD_BUS = new EventManager(FMLJavaModLoadingContext.get().getModEventBus());

    private IEventBus bus;

    private EventManager(IEventBus bus) {
        this.bus = bus;
    }

    public final void register(Object handler) {
        bus.register(handler);
    }

    public final <T extends Event> void register(Consumer<T> handler) {
        bus.addListener(handler);
    }

    public final <T extends GenericEvent<F>, F> void register(Class<F> type, Consumer<T> handler) {
        bus.addGenericListener(type, handler);
    }
}
