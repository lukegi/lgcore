package luhegi.lgcore.loading;

import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.event.lifecycle.*;

public interface ILoadableMod {
    void onSetup(final FMLCommonSetupEvent event);

    void onGatherData(final GatherDataEvent event);

    default void onFingerprintViolation(final FMLFingerprintViolationEvent event) {}

    default void onLoadComplete(final FMLLoadCompleteEvent event) {}

    default void onIMCEnqueue(final InterModEnqueueEvent event) {}

    default void onIMCProcess(final InterModProcessEvent event) {}

    String getID();

    interface IClientLoadable {
        @OnlyIn(Dist.CLIENT)
        void onClientSetup(final FMLClientSetupEvent event);
    }

    interface IServerLoadable {
        @OnlyIn(Dist.DEDICATED_SERVER)
        void onServerSetup(final FMLDedicatedServerSetupEvent event);
    }
}
