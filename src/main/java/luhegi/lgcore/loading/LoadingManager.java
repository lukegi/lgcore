package luhegi.lgcore.loading;

import luhegi.lgcore.event.EventManager;
import luhegi.lgcore.util.Log;

public final class LoadingManager {
    public static final LoadingManager INSTANCE = new LoadingManager();

    public void register(ILoadableMod mod) {
        Log.Info("{} is being loaded", mod.getID());
        EventManager.MOD_BUS.register(mod::onSetup);
        EventManager.MOD_BUS.register(mod::onClientSetup);
        EventManager.MOD_BUS.register(mod::onServerSetup);
        EventManager.MOD_BUS.register(mod::onGatherData);
        EventManager.MOD_BUS.register(mod::onFingerprintViolation);
        EventManager.MOD_BUS.register(mod::onLoadComplete);
        EventManager.MOD_BUS.register(mod::onIMCEnqueue);
        EventManager.MOD_BUS.register(mod::onIMCProcess);
    }

    private LoadingManager() {}

}
