package luhegi.lgcore.util;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

public class Log {
    public static final Log INSTANCE = new Log();
    private static final Logger LOGGER = (Logger) LogManager.getLogger("LG Core");
    private static final Level LEVEL = Level.DEBUG;

    public void setLevel(Level level) {
        LOGGER.setLevel(level);
    }

    public static void Info(String message, Object... args) {
        INSTANCE.info(message, args);
    }

    public static void Debug(String message, Object... args) {
        INSTANCE.debug(message, args);
    }

    public static void Error(String message, Object... args) {
        INSTANCE.error(message, args);
    }

    public void log(Level level, String message, Object... args) {
        if (level == LEVEL || level.isMoreSpecificThan(LEVEL))
            LOGGER.log(level, message, args);
    }

    public void info(String message, Object... args) {
        log(Level.INFO, message, args);
    }

    public void debug(String message, Object... args) {
        log(Level.DEBUG, message, args);
    }

    public void error(String message, Object... args) {
        log(Level.ERROR, message, args);
    }

    private Log() {
    }
}
